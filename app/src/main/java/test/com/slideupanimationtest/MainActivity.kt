package test.com.slideupanimationtest

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewTreeObserver
import android.view.Window
import android.widget.*


class MainActivity : AppCompatActivity() , ViewSlideAnimation.ViewSlideAnimationFinishListener {
    enum class SLIDE_DRICETION { BottomToTop, TopToBottom }
    enum class VIEW_TYPE { TEXTVIEW, IMAGEVIEW }
    val LOGTAG = MainActivity::class.java.name
    // The mArrayList is a list of the views you want to show in succession
    lateinit var mArrayList:ArrayList<SlideUpGuideData>
    val slideViewUpDown = SLIDE_DRICETION.TopToBottom  // control slide direction
    val slideViewBackgroundAlpha = 0.3f  // define background alpha value of slide animation
    val showTextView = VIEW_TYPE.TEXTVIEW  // define slide View to TextView or ImageView

    lateinit var viewSlide:View    // View to Slide
    lateinit var launchViewSlide:Button
    lateinit var ll_backgroundAlpha:LinearLayout
    lateinit var mViewSlideAnim:ViewSlideAnimation
    lateinit var rl_container:RelativeLayout
    var mViewHeight:Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main)

        // button to run sliding
        launchViewSlide = findViewById<Button>(R.id.showLaunchAnim)
        ll_backgroundAlpha = findViewById<LinearLayout>(R.id.ll_backgroundAlpha)
        rl_container = findViewById<RelativeLayout>(R.id.rl_container)


        if (showTextView == VIEW_TYPE.TEXTVIEW) {
            viewSlide = TextView(this).apply {
                textSize = 30f
                gravity = Gravity.CENTER_HORIZONTAL
            }
        } else {
            viewSlide = ImageView(this).apply {
                setBackgroundResource(R.drawable.img_1637267)
            }
        }

        rl_container.apply {
            viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (getHeight() > 0) {
                        mViewHeight = getHeight().toFloat()

                        setSlideObject()
                        initViewSlideAnimation()

                        if (Build.VERSION.SDK_INT >= 16) {
                            viewTreeObserver.removeOnGlobalLayoutListener(this)
                        }
                    }
                    Log.v(LOGTAG,"onGlobalLayout mViewHeight:"+mViewHeight)
                }
            })
        }

            //You do not need the setSlideObject() method if you define the view to slide in xml layout.
//        viewSlide = findViewById<TextView>(R.id.tvHello)
//        viewSlide.visibility = View.GONE

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    /**
     * Makes the View to slide.
     * Basically, TextView and ImageView are possible.
     * It uses View as a parameter,
     * so anything that extends View is possible if you modify code.
     */
    fun setSlideObject() {
        var slideViewStopPosY = 0

        if (viewSlide is TextView )
            slideViewStopPosY = (mViewHeight / 2.3).toInt()
        else if (viewSlide is ImageView)
            slideViewStopPosY = (mViewHeight / 3.3).toInt()

        var mLayoutParams: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        mLayoutParams.setMargins(0, slideViewStopPosY, 0, 0)
        viewSlide.layoutParams = mLayoutParams

        (findViewById<RelativeLayout>(R.id.rl_background)).addView(viewSlide)
    }

    /**
     * initialize slide Animation
     */
    fun initViewSlideAnimation() {
        //
        ll_backgroundAlpha.alpha = slideViewBackgroundAlpha
        mViewSlideAnim = ViewSlideAnimation(viewSlide, this)

        // You can modify values
        // the distance the slides move
        // the duration animation fade in/out
        // Change of alpha value according to fade in / out
        mViewSlideAnim.apply {
            // 1차 slideView 애니메이션에서  View의 현재 위치에서 viewMoveDistanceHeight_1 값만큼 아래에서 부터 viewMoveDistanceHeight_1만큼 slideView
            MOVE_DISTANCE_HEIGHT_1 = mViewHeight/2
            // 2차 slideUp애니메이션에서 1차 애니메이션 종료한 위치에서 viewMoveDistanceHeight_2만큼 slideView
            MOVE_DISTANCE_HEIGHT_2 = -(mViewHeight/2)

            if (slideViewUpDown == SLIDE_DRICETION.BottomToTop) {
                MOVE_DISTANCE_HEIGHT_1 = mViewHeight/2
                MOVE_DISTANCE_HEIGHT_2 =
                        -(MOVE_DISTANCE_HEIGHT_1 + viewSlide.height)
            } else if (slideViewUpDown == SLIDE_DRICETION.TopToBottom){
                MOVE_DISTANCE_HEIGHT_1 = -(mViewHeight/2)
                MOVE_DISTANCE_HEIGHT_2 = mViewHeight/2
            }

            // 1차 slideView Animation 지속시간
            MOVE_DISTANCE_DURATION_1 = 1000L
            // 2차 slideView Animation 지속시간
            MOVE_DISTANCE_DURATION_2 = 1000L
            // fade in duration value
            FADE_IN_DURATION = 1000L
            // fade out duration value
            FADE_OUT_DURATION = 1000L
            // 1차 slideView Animation alpha value FROM
            ALPHA_FROM_1 = 0f
            // 1차 slideView Animation alpha value TO
            ALPHA_TO_1 = 1f
            // 2차 slideView Animation alpha value
            ALPHA_FROM_2 = 1f
            // 2차 slideView Animation alpha value TO
            ALPHA_TO_2 = 0f
        }

        mArrayList = ArrayList<SlideUpGuideData>()

        if (viewSlide is TextView) {
            mArrayList.add(SlideUpGuideData("Money is Great", "http://www.google.com", 0))
            mArrayList.add(SlideUpGuideData("Money is All", "http://www.google.com", 0))
            mArrayList.add(SlideUpGuideData("Money is my life", "http://www.google.com", 0))
        } else if (viewSlide is ImageView) {
            mArrayList.add(SlideUpGuideData("", "http://www.google.com", R.drawable.img_1637267))
            mArrayList.add(SlideUpGuideData("", "http://www.google.com", R.drawable.img_beaver_1639537))
            mArrayList.add(SlideUpGuideData("", "http://www.google.com", R.drawable.img_bluewater_1437302))
        }

        launchViewSlide.setOnClickListener {
            mViewSlideAnim.isNeedFadeInOut = true
            mViewSlideAnim.posShowGuideArray = 0
            mViewSlideAnim.mArrayList = mArrayList
            mViewSlideAnim.isRepeat = false
            mViewSlideAnim.slideView(1000, true, slideViewUpDown)
        }
    }

    override fun viewSlideAnimationFinish(type: Int) {
        Log.v(LOGTAG,"slideViewAnimationFinish")

        if (type == ANIMATION_FINISH_SECOND) {
            mViewSlideAnim.slideView(1000, true, slideViewUpDown)
        } else if (type == ANIMATION_FINISH_FIRST) {

        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}




