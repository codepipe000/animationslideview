package test.com.slideupanimationtest

/**
 * Created by CodePipe000 on 10/03/2019.
 */
data class SlideUpGuideData (val text:String, val link:String, val imageResID:Int)