package test.com.slideupanimationtest

import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.*
import android.widget.ImageView
import android.widget.TextView

const val ANIMATION_FINISH_FIRST = 0
const val ANIMATION_FINISH_SECOND = 1

class ViewSlideAnimation(var mView:View, var mViewSlideAnimationFinishListener:ViewSlideAnimationFinishListener? = null) {

    interface ViewSlideAnimationFinishListener {
        fun viewSlideAnimationFinish(type:Int)
    }

    val LOGTAG = ViewSlideAnimation::class.java.name
    var posShowGuideArray:Int = 0
    var mArrayList = ArrayList<SlideUpGuideData>()
    var isNeedFadeInOut = true
    var isRepeat = false

    // 1차 slideView 애니메이션에서  View의 현재 위치에서 viewMoveDistanceHeight_1 값만큼 아래에서 부터 viewMoveDistanceHeight_1만큼 slideView
    var MOVE_DISTANCE_HEIGHT_1 = 0f
    // 2차 slideUp애니메이션에서 1차 애니메이션 종료한 위치에서 viewMoveDistanceHeight_2만큼 slideView
    var MOVE_DISTANCE_HEIGHT_2 = 0f
    // 1차 slideView Animation 지속시간
    var MOVE_DISTANCE_DURATION_1 = 1000L
    // 2차 slideView Animation 지속시간
    var MOVE_DISTANCE_DURATION_2 = 1000L
    // fade in duration value
    var FADE_IN_DURATION = 1000L
    // fade out duration value
    var FADE_OUT_DURATION = 1000L
    // 1차 slideView Animation alpha value FROM
    var ALPHA_FROM_1 = 0f
    // 1차 slideView Animation alpha value TO
    var ALPHA_TO_1 = 1f
    // 2차 slideView Animation alpha value
    var ALPHA_FROM_2 = 1f
    // 2차 slideView Animation alpha value TO
    var ALPHA_TO_2 = 0f

    lateinit var animate:TranslateAnimation

    /**
     * intervalDelayTime : 1차 애니메이션이 종료된 후 2차 애니메이션 시작전까지 멈춰있을 시간
     */
    fun slideView(intervalDelayTime:Long, sequently:Boolean, isUpDown:MainActivity.SLIDE_DRICETION) {

        mView.visibility = View.VISIBLE

        Log.v(LOGTAG,"posShowGuideArray-1:"+posShowGuideArray)
        if (mArrayList?.size > 0) {
            // By adding a condition, it can be used for any component inheriting View
            if (mView is TextView) {
                (mView as TextView).text = mArrayList[posShowGuideArray].text
                posShowGuideArray++
            } else if (mView is ImageView) {
                (mView as ImageView).setBackgroundResource(mArrayList[posShowGuideArray].imageResID)
                posShowGuideArray++
            }

        }

        animate = TranslateAnimation(0f, 0f, MOVE_DISTANCE_HEIGHT_1, 0f)
        animate.duration = MOVE_DISTANCE_DURATION_1
        animate.fillAfter = true

        val fadeIn = AlphaAnimation(ALPHA_FROM_1, ALPHA_TO_1)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = FADE_IN_DURATION

        val mAnimationSet = AnimationSet(false) //change to false
        if (isNeedFadeInOut)
            mAnimationSet.addAnimation(fadeIn)

        mAnimationSet.addAnimation(animate)
        mView.startAnimation(mAnimationSet)

        mAnimationSet.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                mViewSlideAnimationFinishListener?.viewSlideAnimationFinish(ANIMATION_FINISH_FIRST)
                if (!sequently) {
                    if (posShowGuideArray >= mArrayList.size) {
                        if (isRepeat)
                            posShowGuideArray = 0
                        else
                            return
                    }
                    return
                }
                Handler().postDelayed(Runnable {
                    animate = TranslateAnimation(0f, 0f, 0f, MOVE_DISTANCE_HEIGHT_2)
                    animate.duration = MOVE_DISTANCE_DURATION_2

                    val fadeOut = AlphaAnimation(ALPHA_FROM_2, ALPHA_TO_2)
                    fadeOut.interpolator = AccelerateInterpolator() //and this
                    fadeOut.duration = FADE_OUT_DURATION

                    val mAnimationInnerSet = AnimationSet(false) //change to false
                    if (isNeedFadeInOut)
                        mAnimationInnerSet.addAnimation(fadeOut)

                    mAnimationInnerSet.addAnimation(animate)
                    mAnimationInnerSet.fillAfter = true

                    mView.startAnimation(mAnimationInnerSet)

                    mAnimationInnerSet.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationRepeat(animation: Animation?) {

                        }

                        override fun onAnimationEnd(animation: Animation?) {
                            if (posShowGuideArray >= mArrayList.size) {
                                if (isRepeat)
                                    posShowGuideArray = 0
                                else
                                    return
                            }

                            mViewSlideAnimationFinishListener?.viewSlideAnimationFinish(ANIMATION_FINISH_SECOND)
                        }
                        override fun onAnimationStart(animation: Animation?) {

                        }
                    })
                }, intervalDelayTime)
            }

            override fun onAnimationStart(animation: Animation?) {
                mView.visibility= View.VISIBLE
            }

        })

    }

}

